#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This file handles the learning part of Xylok. For now, only supervised
learning using the decision tree algorithm.

The decision tree computed in an average decision tree: relevant values of Fast
Fourier Transform are averaged to appear as a single *mean frame* in order not
to unbalance the algorithm.

.. note::
    It may deal with incomplete data such as no *Rise* or no *Tilt*
    information, thanks to preprocessing. (However, the decision might not
    be relevant, but this is a user issue.)

.. seealso::
    Standard librairies:
        * :py:mod:`sklearn`
        * :py:mod:`sklearn.preprocessing`
        * :py:mod:`sklearn.tree`
    Xylok:
        * :py:mod:`lib.database`
"""


import sklearn
import sklearn.preprocessing
import sklearn.tree

import lib.database

# Cursor on PostgreSQL database that allows reading/writing data in it
LCURSOR = lib.database.XylokPGCursor()

def generate_learner():
    """
    Returns an averaged DecisionTree

    Parameters:
      - None
    Uses:
      - :py:mod:`sklearn`
      - :py:mod:`sklearn.preprocessing`
      - :py:mod:`sklearn.tree`
    Returns:
      - decision tree and a list of all possible labels (str)
    """

    # Creates an imputer that decides what the missing values are in data
    imp = sklearn.preprocessing.Imputer(missing_values=0.0, strategy="mean", verbose=0, copy=False, axis=1)

    # Retrieves data, list of numbers (translation of labels) and a list of
    # all the possible lables (str) from the PostgreSQL
    datas, answers, possible_answers = LCURSOR.fetch_averaged_sequence_data()

    # The imputer does it's job
    imp.fit(datas)
    datas = imp.transform(datas)

    # Creates the decision tree
    dtree = sklearn.tree.DecisionTreeClassifier()
    dtree.fit(datas, answers)

    return dtree, possible_answers
