#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This module offers a connector to the database:

  - ``XylokPGCursor`` which is dedicated to the learning part
    of Xylok. For now, it just compute averaged data.
"""
import psycopg2
import psycopg2.extras

import itertools

class XylokPGCursor(object):
    """
    This class describes a psycopg2 cursor to the database.

    It is a DictCursor, but uses the tuples for learning
    functionalities because dict order of result
    is not guaranteed.
    """

    def __init__(self):
        """
        Starts a psycopg2 cursor and populates:
          - ``self._conn``
          - ``self._cur``

        Parameters:
          - ``self``

        Returns:
          - nothing

        """
        self._conn = psycopg2.connect(database='itpp')
        self._conn.set_session(autocommit=True)
        self._cur = self._conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    def check_sequence(self, seq):
        """
        Checks if seq is already recorded in database.

        Parameters:
          - ``self``
          - ``seq`` : sequence object (:py:mod:`sequence`)

        Returns:
          - int

        """
        verif_query = """SELECT
    id, label
FROM
    sequences
WHERE
    sequence=%s;"""
        self._cur.execute(verif_query, (seq.sequence,))
        ret = self._cur.fetchone()
        if ret:
            return ret[0], ret[1]
        else:
            return 0, None

    def store_sequence(self, seq):
        """
        Stores the sequence data in database

        Parameters:
          - ``self``
          - ``seq`` : sequence object (:py:mod:`sequence`)

        Returns:
          - nothing

        """

        # This query is designed to add the sequence metadata in the database.
        # We give explicitly the columns of the table we want to fill, and we also give the dict keys for the VALUES,
        # so we only have to give seq to cur.execute() as if seq was a dict.
        main_seq_query = """INSERT INTO
    sequences
        (accession, name, description, sequence, label, alphabet, alpha, sliding, centering, trx_scale_path)
    VALUES
        (%(accession)s, %(name)s, %(description)s, %(sequence)s, %(label)s, %(alphabet)s,\
            %(alpha)s, %(sliding)s, %(centering)s, %(trx_scale_path)s)
    RETURNING
        id;"""

        self._cur.execute(main_seq_query, seq)
        # The query returns the id of the new recorded data. Will be useful for the other queries.
        seq_id = self._cur.fetchone()[0]

        # We do a record for each helicoidal param, recording all the frames in the good table.
        for helicoidal_parameter in seq.mdd:
            for (frame_num, values) in seq.mdd[helicoidal_parameter].iteritems():
                # this query is formatted twice, once for data outside from value dict and for the table name,
                # once again for the dict value itself. This is why there is %%(attr)s instead of %(attr)s.
                md_query = """INSERT INTO
        md_%s
            (seq_id, frame_num, complete_peak_freq, complete_peak, complete_size, center_peak_freq,\
                center_peak, center_size, sliding_peak_freq, sliding_peak, sliding_size)
        VALUES
            (%s, %s, %%(complete_peak_freq)s, %%(complete_peak)s, %%(complete_size)s, %%(center_peak_freq)s,\
                %%(center_peak)s, %%(center_size)s, %%(sliding_peak_freq)s, %%(sliding_peak)s,\
                %%(sliding_size)s);""" % (helicoidal_parameter, seq_id, frame_num)

                # We execute the query.
                self._cur.execute(md_query, values)

        # Trx data.
        trx_query = """INSERT INTO
    trx
        (seq_id, complete_peak_freq, complete_peak, complete_size, center_peak_freq,\
            center_peak, center_size, sliding_peak_freq, sliding_peak, sliding_size)
    VALUES
        (%(seq_id)s, %(complete_peak_freq)s, %(complete_peak)s, %(complete_size)s,\
            %(center_peak_freq)s, %(center_peak)s, %(center_size)s, %(sliding_peak_freq)s, %(sliding_peak)s, %(sliding_size)s);"""
        dic_to_sql = dict(seq.trx)
        dic_to_sql.update({'seq_id': seq_id})
        self._cur.execute(trx_query, dic_to_sql)

        #Correlation data
        for (correl_types, bunch_of_data) in seq.correlation.iteritems():
            type_a, type_b = correl_types.split("/")
            for (frame_num, data) in bunch_of_data.iteritems():
                corr_query = """INSERT INTO
            correlations
                (seq_id, frame_num, type_a, type_b, spearman_complete, spearman_center, pearson_complete, pearson_center)
            VALUES
                (%(seq_id)s, %(frame_num)s, %(type_a)s, %(type_b)s, %(spearman_complete)s,\
                    %(spearman_center)s, %(pearson_complete)s, %(pearson_center)s);"""
                dic_to_sql = dict(data)
                dic_to_sql.update({
                    "frame_num": frame_num,
                    "seq_id": seq_id,
                    "type_a": type_a,
                    "type_b": type_b,
                    })
                self._cur.execute(corr_query, dic_to_sql)
        return seq_id

    def remove_seq(self, seq_id):
        """
        Removes a sequence from database

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)

        Returns:
          - nothing

        """

        for md_type in ["rise", "roll", "shift", "slide", "tilt", "twist"]:
            self._cur.execute("DELETE FROM md_%s WHERE seq_id=%s" % (md_type, seq_id))
        self._cur.execute('DELETE FROM trx WHERE seq_id=%s', (seq_id,))
        self._cur.execute('DELETE FROM correlations WHERE seq_id=%s', (seq_id,))
        self._cur.execute('DELETE FROM sequences WHERE id=%s', (seq_id,))
        return self._cur.rowcount

    def fetch_averaged_specific_sequence(self, seq_id):
        """
        Retreive averaged data about a sequence.

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)

        Returns:
          - ``data_list`` : a list of floats, which describes the sequence
              for the :py:mod:`sklearn.tree.DesicionTree`.

        """
        data_list = []
        data_list.extend(self.fetch_trx_data(seq_id))
        data_list.extend(self.fetch_averaged_md_data(seq_id))
        data_list.extend(self.fetch_averaged_correlations(seq_id))

        return data_list

    def fetch_averaged_sequence_data(self):
        """
        Retreive all averaged data for all sequences in database

        Parameters:
          - ``self``

        Returns:
          - ``data_list`` : a list of list of floats, containing all data
            about sequences.
          - ``answers`` : all labels corresponding to sequences, int-formatted
          - ``possible_answers`` : a list to translate back int into labels.

        """

        seq_query = """SELECT
    id, label, name
FROM
    sequences
ORDER BY
    id
ASC;"""
        self._cur.execute(seq_query)
        sequences = self._cur.fetchall()

        # Build the list of possible answers and corresponding int sequence
        # as sklearn isn't able to manage non numeric data.
        possible_answers = list(set([seq[1] for seq in sequences]))
        answers = [possible_answers.index(seq[1]) for seq in sequences]
        data_list = []
        i = 0
        for sequence in sequences:
            seq_id = sequence[0]
            data_list.append([])
            data_list[i].extend(self.fetch_trx_data(seq_id))
            data_list[i].extend(self.fetch_averaged_md_data(seq_id))
            data_list[i].extend(self.fetch_averaged_correlations(seq_id))
            i += 1
        return data_list, answers, possible_answers

    def fetch_averaged_md_data(self, seq_id):
        """
        Retreives the md part for a sequence. Missing values are padded with
        0.0

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)

        Returns:
          - ``data_list`` : a list of floats, which describes the sequence
            md data.

        """

        data_list = []

        for md_type in ["rise", "roll", "shift", "slide", "tilt", "twist"]:
            md_query = """SELECT
    avg(complete_peak_freq), avg(complete_peak), avg(complete_size), avg(center_peak_freq),\
    avg(center_peak), avg(center_size), avg(sliding_peak_freq), avg(sliding_peak), avg(sliding_size)
FROM
    md_%s
WHERE
    seq_id = %s
GROUP BY
    seq_id;""" % (md_type, seq_id)
            self._cur.execute(md_query)
            t = self._cur.fetchone()
            if not t:
                t = [0.0]*9
            data_list.append(t)

        # Transforms [[a, b, c], [d, e], [f, g, h]] in [a, b, c, d, e, f, g, h]
        return [elem for elem in itertools.chain(*data_list)]

    def fetch_averaged_correlations(self, seq_id):
        """
        Fetches data from correlation table for a sequence. If some values are missing,
        set values to 0.0

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)

        Returns:
          - ``data_list`` : a list of floats, which describes the sequence
              correlation data.

        """

        data_dict = {}

        # Here, we build an ordered list of all couples of type_a/type_b.
        # It represents 15 couples, which means 15 lists for correlation
        # data.
        types_a = ["rise", "roll", "shift", "slide", "tilt", "twist"]
        types_b = ["roll", "shift", "slide", "tilt", "twist"]
        couples = []
        for type_a in types_a:
            if type_a in types_b:
                types_b.remove(type_a)
            for type_b in types_b:
                couples.append("%s/%s" % (type_a, type_b))
        del types_a, types_b

        # Prefill the data dict with interesting keys.
        for couple in couples:
            data_dict[couple] = []

        corr_query="""SELECT
    type_a, type_b, avg(spearman_complete::int::float), avg(spearman_center::int::float), avg(pearson_complete::int::float), avg(pearson_center::int::float)
FROM
    correlations
WHERE
    seq_id = %s
GROUP BY
    type_a, type_b
ORDER BY
    type_a, type_b
ASC;""" % (seq_id, )
        self._cur.execute(corr_query)
        data = self._cur.fetchall()

        # Fill the dict using the preinserted keys.
        for tpl in data:
            data_dict["%s/%s" % (tpl[0], tpl[1])].extend(tpl[2:])
        del data

        data = []

        # Missing data are padded.
        # It's easy since all is averaged.
        for couple in couples:
            if not data_dict[couple]:
                data.extend([0.0]*4)
            else:
                data.extend(data_dict[couple])
        return data

    def fetch_trx_data(self, seq_id):
        """
        Fetches trx values for sequence.

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)

        Returns:
          - ``data_list`` : a list of floats, which describes the sequence
              correlation data.

        """
        trx_query = """SELECT
    complete_peak_freq, complete_peak, complete_size, center_peak_freq, center_peak, center_size, sliding_peak_freq, sliding_peak, sliding_size
FROM
    trx
WHERE
    seq_id = %s;""" % (seq_id,)
        self._cur.execute(trx_query)
        return list(self._cur.fetchone())

    def label_sequence(self, seq_id, label):
        """
        Label the selected sequence

        Parameters:
          - ``self``
          - ``seq_id`` : sequence id (int)
          - ``label`` : the label for sequence (str)

        Returns:
          - nothing

        """
        update_query = """UPDATE
    sequences
SET
    label = %s
WHERE
    id = %s;"""
        self._cur.execute(update_query, (label, seq_id))
