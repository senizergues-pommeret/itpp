#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This module contains every function required to compute the Fast Fourier
Transform on Xylok data structures.

Requires:
  - :py:mod:`numpy`
  - :py:mod:`lib.XylokExceptions`
"""

import numpy

from lib.XylokExceptions import ShiftOutOfRange

def split_frame(frame):
    """
    Splits a frame into 2 lists in a way that guarantees the proper order in
    both x_values and y_values.

    Parameters:
      - ``frame`` : a dictionary where keys are positions and values are either
                    TRX or helicoidal values

    Returns:
      - The frame number of the used ``frame`` and 2 lists, the 1st one
        containing the positions and the 2nd one containing the values.
    """
    x_values = []
    y_values = []

    for (key, value) in frame.iteritems():
        # Isinstance is designed to check if key is of type int
        # So that we get only (key, value) that are (position, value) and no
        # ("frame", n)
        if isinstance(key, int):
            x_values.append(key)
            y_values.append(value)

    # Because TRX data don't have a frame number (so frame['frame'] will raise
    # an error)
    frame_number = frame.get("frame", -1)

    # Making sure x_values and y_values are both in good order (order of
    # elements in dict is not guaranteed)
    #
    # -*-      Example         -*-
    #
    # In [20]: a=[4, 3, 1, 2]
    #
    # In [21]: b=["a", "f", "c", "u"]
    #
    # In [22]: zip(a, b)
    # Out[22]: [(4, 'a'), (3, 'f'), (1, 'c'), (2, 'u')]
    #
    # In [23]: sorted(zip(a, b))
    # Out[23]: [(1, 'c'), (2, 'u'), (3, 'f'), (4, 'a')]
    #
    # In [24]: zip(*sorted(zip(a, b)))
    # Out[24]: [(1, 2, 3, 4), ('c', 'u', 'f', 'a')]
    #
    # In [25]: a, b = map(list, zip(*sorted(zip(a, b))))
    #
    # In [26]: a
    # Out[26]: [1, 2, 3, 4]
    #
    # In [27]: b
    # Out[27]: ['c', 'u', 'f', 'a']
    #
# Use of comprehensive list instead of map 'cause pylint is not kind
    return frame_number, [numpy.array(elem) for elem in zip(*sorted(zip(x_values, y_values)))]

def fft(frame, centering=None):
    """
    Computes the Fast Fourier Transform for the given ``frame``.

    Parameters:
      - ``frame``     : a dictionary where keys are positions and values are
                       either TRX or helicoidal values
      - ``centering`` : int, number of bp considered as the sequence center

    Uses:
      - :py:meth:`split_frame`
      - :py:mod:`numpy`

    Returns:
     - a tuple (int: frame number, list: frequencies, list: fft values)

    """
    # Retrieves data
    frame_number, (x_values, y_values) = split_frame(frame)

    # Computes max of positive fft freqs
    m_fft = (y_values.size + 1)/2

    # Specific to centering
    if centering is not None:
        offset = int((len(x_values) - centering)/2)
        x_values = x_values[offset:offset + centering]
        y_values = y_values[offset:offset + centering]

    # Creates frequency space, x_values[1] - x_values[0] is the typical delta
    # in x_values.
    freqs = numpy.fft.fftfreq(y_values.size, x_values[1] - x_values[0])[0:m_fft]

    # Computes the fft on the signal, retrieves real part.
    transform = abs(numpy.fft.fft(y_values))[0:m_fft]

    return (frame_number, list(freqs), list(transform))

def get_noticeable_data(fft_data):
    """
    Fetches (argmax, max) in ``fft_data``, and the size of the peak associated
    with this max.

    Parameters:
      - ``fft_data`` : a tuple that contains the frame number (int) and 2 lists
                       one with the frequencies and the other one with the fft
                       values.

    Returns:
      - a tuple of 3 elements, peak_freq (frequency of the greatest peak), peak
        (signal intensity at the greatest peak) and size (peak width)

    """
    # http://stackoverflow.com/questions/5893163/underscore-in-python
    _, freqs, transform = fft_data

    # Reverses the transform in order to retrieve the greater peak_index
    # (greatest frequency is more interesting: smallest period)
    # Getting rid of the first points (frequencies near 0 are full of noise)
    reverse_transform = list(transform)[2:]
    reverse_transform.reverse()

    # Computes the peak_index and the peak (peak intensity)
    peak_index, peak = reverse_transform.index(max(reverse_transform)), max(reverse_transform)

    # Reverses the peak_index since it was obtained from the reversed list
    peak_index = len(transform) - 1 - peak_index

    # Retrieves the peak_frequency
    peak_freq = freqs[peak_index]

    # (This is so dirty.)
    # Finds peak width that is defined as the sign change of the derivative
    left_index = 0
    right_index = len(freqs) - 1
    target_value = peak
    # Screening right part of the peak for right_index (inflection point)
    for index in xrange(peak_index + 1, len(freqs)):
        if transform[index] > target_value:
            right_index = index - 1
            break
        else:
            target_value = transform[index]

    target_value = peak
    # Screening left part of the peak for left_index (inflection point)
    for index in xrange(peak_index - 1, -1, -1):
        if transform[index] > target_value:
            left_index = index + 1
            break
        else:
            target_value = transform[index]

    # Gets actual peak width
    size = freqs[right_index] - freqs[left_index]

    return (peak_freq, peak, size)

def sliding_fft(frame, window=72):
    """
    Computes the Fast Fourier Transform of a "sliding" frame. A sliding frame
    is a frame where the value at position i is the mean value of the values
    of the actual frame from position i to position i+``window``.

    It computes the Fast Fourier Transform on smoothed data.

    Parameters:
      - ``frame``  : a dictionary where keys are positions and values are
                     either TRX or helicoidal values
      - ``window`` : int, number of bp considered as the sequence center

    Uses:
      - :py:meth:`split_frame`
      - :py:class:`lib.XylokExceptions:ShiftOutOfRange`
      - :py:meth:`fft`

    Returns:
      - a tuple (int: frame number, list: frequencies, list: fft values)
    """
    # Creating lists for the to-be-smoothed data
    modified_x = []
    modified_y = []

    # Retrieves useful data from the frame
    # http://stackoverflow.com/questions/5893163/underscore-in-python
    frame_number, (_, y_values) = split_frame(frame)

    # It makes no sense to smooth data if the ``window`` is larger than the
    # sequence!
    if len(y_values) <= window:
        raise ShiftOutOfRange("Choosen shift (%r) is too long for query sequence (which len is %r)." % (window, len(y_values)))

    # Computes the new lists with smoothed data
    for index in xrange(0, len(y_values) - window):
        modified_x.append(index)
        # Rounds to 3: significant numbers
        modified_y.append(numpy.round(numpy.mean(y_values[index:index+window]), 3))

    # Creates a new "sliding" frame
    modified_frame = {i: j for (i, j) in zip(modified_x, modified_y)}
    modified_frame['frame'] = frame_number

    return fft(modified_frame)

#def plot(fft_list, fftc_list, ffts_list, plot_freq, helicoidal_parameter):
#    """
#    Plots fft graphs
#    """
#
#    pass
