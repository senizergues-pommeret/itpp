#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This file holds the definitions of Xylok's specific errors.

All of them inherit :py:class:`XylokError`.
"""

class XylokError(Exception):
    """
    Base class for exceptions.
    """
    def __init__(self, msg=""):
        super(XylokError, self).__init__(msg)

class NoMatch(XylokError):
    """
    Exception raised when no match is found by :py:meth:`lib.trx.pair_score`

    Parameters:
      - ``msg`` : error message (str)
    """

    def __init__(self, msg):
        super(NoMatch, self).__init__(msg)

class ShiftOutOfRange(XylokError):
    """
    Exception raised when shift is too long for the used query sequence in
    :py:meth:`lib.fft_tools.sliding_fft`

    Parameters:
      - ``msg`` : error message (str)
    """

    def __init__(self, msg):
        super(ShiftOutOfRange, self).__init__(msg)

class NoFastaProvided(XylokError):
    """
    Exception raised when no fasta file is found in the data directory by
    :py:mod:`create_db`

    Parameters:
      - ``msg`` : error message (str)
    """

    def __init__(self, msg):
        super(NoFastaProvided, self).__init__(msg)

class NoLabelProvided(XylokError):
    """
    Exception raised when no label file is found in the data directory by
    :py:mod:`create_db`

    Parameters:
      - ``msg`` : error message (str)
    """

    def __init__(self, msg):
        super(NoLabelProvided, self).__init__(msg)
