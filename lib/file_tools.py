#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This file contains the functions required to load Xylok data from files.

.. note::
    MD file must look like:
       frame  C16/C17  C17/G18  G18/A19 ...
           0     -1.0      9.0     -1.8
           1     -4.4     -7.6     -0.1
           2     -6.4      1.0     -6.0
           3     -3.6     -1.1     -2.5

.. seealso::
    Standard librairies:
        * :py:mod:`re`
        * :py:mod:`Bio.SeqIO`
        * :py:mod:`Bio.Alphabet.IUPAC`
"""

import re
from Bio import SeqIO
from Bio.Alphabet import IUPAC

def load_fasta(sequence_alphabet, path):
    """
    Loads a fasta sequence from a file, using its alphabet and returns a record
    with all the information in the file.

    Parameters:
      - ``sequence_alphabet`` : str (dna, rna, prot)
      - ``path``              : path to FASTA file
    Uses:
      - :py:mod:`Bio.SeqIO`
      - :py:mod:`Bio.Alphabet`
    Returns:
      - an object of type :py:class:`Bio.SeqRecord.SeqRecord`
    """

    if sequence_alphabet.lower() == 'dna':
        sequence_alphabet = IUPAC.unambiguous_dna
    else:
        # Maybe someday, RNA analysis will be enabled
        raise NotImplementedError

    return SeqIO.read(path, format="fasta", alphabet=sequence_alphabet)

def load_md_data(path):
    """
    Loads a post-processed MD file into a list of dictionaries (each frame has
    its dictionary).

    .. note::
        MD file must look like:
           frame  C16/C17  C17/G18  G18/A19 ...
               0     -1.0      9.0     -1.8
               1     -4.4     -7.6     -0.1
               2     -6.4      1.0     -6.0
               3     -3.6     -1.1     -2.5

    Parameters:
      - ``path`` : str, path to the file
    Returns:
      - a list of dictionaries (each dictionary is a MD frame)
        [ ... {"frame": frame#, ..., position_i: value_i, ...} ...]
    """

    positions = []
    output = []

    with open(path, 'r') as handle:
        # Retrieves nucleotides positions from the "header" line
        # The header is the first line
        header = handle.readline()
        # Getting rid of the "\n" at the end of the line and spliting to get
        # this kind of list ['frame', 'C16/C17', 'C17/G18', ..., 'A130/G131']
        header = header.replace('\n', '').split()

        # Working on 'frame' (header[0]) or string like 'C17/G18'
        for char in header:
            # Uses the string at the left side of the '/'
            positions.append(re.sub(r'[A-Z]', '', char.split('/')[0]))

        # Retrieves the last element (never on the left side of the '/')
        positions.append(re.sub(r'[A-Z]', '', header[-1].split('/')[1]))

        # Getting rid of 'frame' (first word of the file)
        # And getting rid of the last position.
        # http://stackoverflow.com/questions/5893163/underscore-in-python
        _ = positions.pop(0)
        _ = positions.pop(-1)

        # Starting to work on the 'real' data (frame 0)
        line = handle.readline()

        # Processing every data line in the file
        while line:
            # Getting a list of the helicoidal parameter value
            # ['frame number', '-10.9', ..., '5.2']
            line = line.replace('\n', '').split()

            # Casting the frame number to int and the helicoidal parameter
            # values to float for further use
            line = [int(line[0])] + [float(x) for x in line[1:]]

            # Creating a dictionary looking like
            # {..., 'position_i': 'helicoidal_parameter_value_i', ...}
            # There is a shift between positions (contains frame number) and
            # line (doesn't)
            newline = {int(positions[x]): line[x+1] for x in xrange(len(positions))}
            # Inserting the "frame": frame number at first position
            # (for developper convenience)
            newline["frame"] = line[0]

            # Saving the processed line to the output
            output.append(newline)
            # Next line
            line = handle.readline()
    return output
