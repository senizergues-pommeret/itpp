#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This module defines the functions needed to process a sequence into a
dictionary for further use.
"""

import os
import re

from lib.XylokExceptions import NoMatch

# This dictionary is there to translate TRX scale
NUCLEOTIDE_MAP = {"Y" : "[CT]", "R" : "[AG]"}

# Default TRX scale file
SCALE_FILE = os.path.join(os.path.dirname(__file__), '../data/trx_scale')

def translate_trx_scale(pattern, n_map):
    """
    Translates a matching pattern so that it can be used with regexps.
    It uses a nucleotide map.

    Example: "YCGR" becomes "[CT]CG[AG]"

    Parameters:
      - ``pattern`` : string (e.g. "YCGR")
      - ``n_map``   : dictionary

    Returns:
      - a string
    """
    # NB: D.get(k[,d]) -> D[k] if k in D, else d.  d defaults to None.
    return "".join([n_map.get(i, i) for i in pattern])

def parse_trx_scale(path_to_scale):
    """
    Parses a TRX scale file and creates a dictionary with the scale.

    NB: TRX scale file must look like :py:data:`data.trx_scale`

    Parameters:
      - ``path_to_scale`` : string, path to the TRX scale to be used

    Uses:
      - :py:meth:`translate_trx_scale`

    Returns:
      - a TRX dictionary that looks like {..., '[CT]CG[AG]': 59.0, ...}
    """
    # Initializes a new empty dictionary scale
    trx = {}

    with open(path_to_scale, "r") as handle:
        line = handle.readline()
        # Processing every data line
        while line:
            # Not taking into account empty lines and lines starting with
            # "#" (comments)
            if line.strip() != "" and (line.startswith("#") == False):
                # line.split()[0]: the nucleotide string (2 or 4 letters) that
                #                  needs translation, using
                #                  :py:module:lib/trx_tools:translate_trx_scale
                # line.split()[1]: the associated value that needs to be
                #                  cast into float
                trx[translate_trx_scale(line.split()[0], NUCLEOTIDE_MAP)] = float(line.split()[1])
            line = handle.readline()

    # At this point, we have a TRX dictionary that looks like this:
    # trx = {'AA' : 5.0, ..., '[CT]CG[AG]': 59.0, ...}
    return trx

def pair_score(string, trx):
    """
    Tries to match ``string`` to the pattern stored in the TRX scale
    dictionary and returns the value associated to ``string`` in the scale.

    Parameters:
      - ``string`` : 2 or 4 letters long string
      - ``trx``    : TRX scale dictionary

    Uses:
      - :py:mod:`re`

    Returns:
      - a float

    May raise:
      - NoMatch
    """
    # Initializes a new empty list for all the pattern that match the query
    # pattern
    potential = []

    if len(string) == 4:
        # Iteration on all keys of the TRX dictionary
        for pattern in trx.keys():
            # Test whether the 2 central nucleotides are in the pattern
            if re.search(string[1:3], pattern):
                # Only one pattern 2 letters long is possible and it must be
                # kept in case the tetranucleotides don't match the whole
                # string
                if len(pattern) == 2:
                    potential.append(pattern)
                # Only one 4 letters long pattern is possible.
                elif re.search(string[0], pattern[1:3]) and re.search(string[-1], pattern[7:9]):
                    potential.append(pattern)
    elif len(string) == 2:
        # Iteration on the keys of the TRX dictionary which have a length == 2
        for pattern in [i for i in trx.keys() if len(i) == 2]:
            if re.search(string, pattern):
                potential.append(pattern)


    if not potential:
        # If the potential list is empty, clearly something gone wrong
        raise NoMatch("No match found, please check your sequence and TRX scale.")
    else:
        # There might be up to 2 patterns in `potential`: a 2 letter long and a
        # 4 letter long. The selected pattern is the longest one (specificity)
        return trx[max(potential)]


def match(sequence, trx, center=None):
    """
    Translates a raw ``sequence`` into a dictionary which structure is:
    {..., position: trx_value, ...}

    NB: position i studies phosphate between nucleotide i and nucleotide i+1.

    Parameters:
      - ``sequence`` : SeqRecord
      - ``trx``      : TRX scale dictionary
      - ``center``   : int that gives the number of bp that are considered as
                       the sequence center (default: None)
    Uses:
      - :py:meth:`pair_score`

    Returns:
      - a dictionary
    """
    sequence_trx = {}
    # Use of str(sequence) instead of sequence.tostring() because the
    # documentation says so nowaday (25/11/2014)
    sequence = str(sequence)

    if center is not None:
        length = center
        offset = int((len(sequence) - length)/2)
    else:
        length = len(sequence) - 1
        offset = 0

    for position in xrange(offset, offset+length):
        if position == 0:
            sequence_trx[position] = pair_score(sequence[position:position+2], trx)
        elif position == len(sequence)-2:
            sequence_trx[position] = pair_score(sequence[position:position+2], trx)
        else:
            sequence_trx[position] = pair_score(sequence[position-1:position+3], trx)
    return sequence_trx
