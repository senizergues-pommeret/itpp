#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Generates the decision tree associated to the PostqreSQL database that holds
all the data upon the sequences of the training set.

If the query sequence is already stored in the database, the program exits with
a 42 error code.

If the query sequence is not in the database, its noticeable values are
computed and then is processed by the learning algorithm.


.. warning::
    Requires a populated PostgreSQL database in order to run properly.
    The use of :py:mod:`populate_db` is highly recommanded.

.. seealso::
    Standard librairies:
        * :py:mod:`argparse`
        * :py:mod:`os`
        * :py:mod:`sys`
    Xylok:
        * :py:mod:`lib.learning`
        * :py:mod:`lib.database`
        * :py:mod:`lib.trx`
        * :py:mod:`sequence`

"""


import argparse
from argparse import RawDescriptionHelpFormatter
import os
import sys

import lib.learning
import lib.database
import lib.trx as trx_lib
import sequence


if __name__ == "__main__":
    DBCURSOR = lib.database.XylokPGCursor()

    # -*-          Parsing command-line arguments using argparse          -*- #

    # TEXT is a string that holds the description of the program
    TEXT = """This program creates adds a sequence to the database and find the appropriate label for it."""


    # Creates the parser
    PARSER = argparse.ArgumentParser(description=TEXT, formatter_class=RawDescriptionHelpFormatter)

    # Fills the parser with the program arguments
    PARSER.add_argument("-a", "--alphabet", type=str, default="dna", help="[str] sequences alphabet (dna, rna, prot), currently only dna is implemented (default: 'dna')", action="store")
    PARSER.add_argument("-A", "--alpha", type=float, default=0.05, help="[float] alpha parameter of the Student table that is to be used in the statistical analysis (default: 0.05)", action="store")
    PARSER.add_argument("-c", "--centering", type=int, default=72, help="[int] number of bp that are to be considered as the center of the sequence (default: 72)", action="store")
    PARSER.add_argument("-s", "--sliding", type=int, default=72, help="[int] number of bp that are to be included in the smoothing window (default: 72)", action="store")
    PARSER.add_argument("-t", "--trx-scale-file", type=str, default=trx_lib.SCALE_FILE, help="[str] path to trx scale file (default: trx_lib.SCALE_FILE)", action="store")
    PARSER.add_argument("-g", "--graph", type=int, default=None, help="[int] number of graphs that are to be plotted, currently not implemented (default: None)", action="store")
    PARSER.add_argument("datadir", type=str, help="[str] path to the data directory", action="store")

    # Parses the arguments
    ARGS = PARSER.parse_args()

    # Obviously, datadir is mandatory, if none is given, nothing can be done
    if not ARGS.datadir:
        raise EnvironmentError("You have to give a data directory.")

    cur_dir = os.path.basename(os.path.normpath(ARGS.datadir))

    # -*-                     Populating the database                     -*- #

    md_parameters = {}
    for filepath in os.listdir(ARGS.datadir):
        if filepath == "sequence.fasta":
            fasta_file = os.path.join(ARGS.datadir, filepath)
        if ".dat" in filepath:
            md_parameters[filepath.replace(".dat", '')] = os.path.join(ARGS.datadir, filepath)

    # Fasta file is mandatory (a sequence is required)
    if fasta_file is None:
        raise NoFastaProvided("There is no fasta file in %r" % (cur_dir,))

    # Tells the users that something is happening (some users do like that)
    print "Processing %r" % (cur_dir,)

    # Creates a Sequence object
    seq = sequence.Sequence(fasta_file, md_parameters, alphabet=ARGS.alphabet, trx_scale_path=ARGS.trx_scale_file, sliding=ARGS.sliding, centering=ARGS.centering, alpha=ARGS.alpha, graph=ARGS.graph)
    seq_id, label = DBCURSOR.check_sequence(seq)
    if seq_id:
        print "This sequence is already in database (id=%s, label=%s)." % (seq_id, label)
        sys.exit(42)

    # We have to build the DT only if we are certain that the sequence is
    # not in the db, as it could alter the result of the DT.
    DT, PA = lib.learning.generate_learner()
    seq.do_analysis()

    # Tells the users that something is happening (some users do like that)
    print "Adding data stored in %r in the database" % (cur_dir,)

    # Stores the sequence "seq" in the database
    seq_id = DBCURSOR.store_sequence(seq)

    # Done, moving on to next directory
    print "Data stored in %s added to database under id %s" % (cur_dir, seq_id)

    data = DBCURSOR.fetch_averaged_specific_sequence(seq_id)
    label = PA[DT.predict(data)[0]]
    print "Guessed label for sequence %s : %s" % (cur_dir, label)
    DBCURSOR.label_sequence(seq_id, label)
