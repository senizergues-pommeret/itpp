ECHO=echo
BEGPWD=$(shell pwd -P)

.PHONY: clean rmpyc purgedoc

tarball: pommeret_senizergues.tar.gz

pommeret_senizergues.tar.gz: clean
	@${ECHO} Making source tarball...
	cd /tmp; tar -C ${BEGPWD} --exclude-vcs -czv -f pommeret_senizergues.tar.gz .; mv pommeret_senizergues.tar.gz ${BEGPWD}
	@${ECHO} Should be okay, now.

clean: rmpyc purgedoc
	find . -iname "pommeret_senizergues.tar.gz" -delete

rmpyc:
	@${ECHO} -n Purging all .pyc files...
	@find . -iname "*.pyc" -delete
	@${ECHO} " done."

purgedoc:
	@${ECHO} Purging documentation...
	@make -C doc clean

doc: doc/html doc/latexpdf

doc/html:
	@${ECHO} Building html documentation...
	@make -C doc html

doc/latexpdf:
	@${ECHO} Building pdflatex documentation...
	@make -C doc latexpdf

create_db:
	${BEGPWD}/sql/create_db.sh chopopope
