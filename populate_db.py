#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
This program populates a pre-existing PostgreSQL database with the results.

The datadir tree must look like
    datadir
         - dir1
              - group (mandatory)
              - sequence.fasta (mandatory)
              - rise.dat
              - roll.dat
              - tilt.dat
              - twist.dat
              - shift.dat
              - slide.dat
         - ...

.. warning::
    This program requires a PostgreSQL database in order to work properly!
    Please run :py:data:`sql.create_db.sh` with your PAM username as an
    argument or create a PostgreSQL using the bdd schema stored in
    :py:data:`sql.create_db.sql`.


.. seealso::
    Standard libraries:
        * :py:mod:`os`
        * :py:mod:`argparse`
        * :py:mod:`sequence`
    Xylok:
        * :py:mod:`lib.XylokExceptions`
        * :py:mod:`lib.trx`
        * :py:mod:`lib.database`

"""

import os
import argparse
from argparse import RawDescriptionHelpFormatter

import sequence
from lib.XylokExceptions import NoFastaProvided
from lib.XylokExceptions import NoLabelProvided
import lib.trx as trx_lib
import lib.database as database

if __name__ == "__main__":
    DBCURSOR = database.XylokPGCursor()

    # -*-          Parsing command-line arguments using argparse          -*- #

    # TEXT is a string that holds the description of the program
    TEXT = """This program creates a postgresql database that holds the following data:
    - sequence information (name, accession number, sequence alphabet, description, etc)
    - analysis parameters (Student table's alpha, number of bp in center, smooth window, etc)
    - relevant information on the TRX signal FFT
    - relevant information on the MD helicoidal parameters FFT
    - Spearman and Pearson correlations between every helicoidal parameters for every frame of the MD

The data directory must be organized this way:
    datadir
         \\-dir1
              \\-group (mandatory)
              \\-sequence.fasta (mandatory)
              \\-rise.dat
              \\-roll.dat
              \\-tilt.dat
              \\-twist.dat
              \\-shift.dat
              \\-slide.dat
         \\-...

A .dat file must me organized this way:
   frame  C16/C17  C17/G18  G18/A19 ...
       0     -1.0      9.0     -1.8
       1     -4.4     -7.6     -0.1
       2     -6.4      1.0     -6.0
       3     -3.6     -1.1     -2.5

"""


    # Creates the parser
    PARSER = argparse.ArgumentParser(description=TEXT, formatter_class=RawDescriptionHelpFormatter)

    # Fills the parser with the program arguments
    PARSER.add_argument("-a", "--alphabet", type=str, default="dna", help="[str] sequences alphabet (dna, rna, prot), currently only dna is implemented (default: 'dna')", action="store")
    PARSER.add_argument("-A", "--alpha", type=float, default=0.05, help="[float] alpha parameter of the Student table that is to be used in the statistical analysis (default: 0.05)", action="store")
    PARSER.add_argument("-c", "--centering", type=int, default=72, help="[int] number of bp that are to be considered as the center of the sequence (default: 72)", action="store")
    PARSER.add_argument("-s", "--sliding", type=int, default=72, help="[int] number of bp that are to be included in the smoothing window (default: 72)", action="store")
    PARSER.add_argument("-t", "--trx-scale-file", type=str, default=trx_lib.SCALE_FILE, help="[str] path to trx scale file (default: trx_lib.SCALE_FILE)", action="store")
    PARSER.add_argument("-g", "--graph", type=int, default=None, help="[int] number of graphs that are to be plotted, currently not implemented (default: None)", action="store")
    PARSER.add_argument("datadir", type=str, help="[str] path to the data directory", action="store")

    # Parses the arguments
    ARGS = PARSER.parse_args()

    # Obviously, datadir is mandatory, if none is given, nothing can be done
    if not ARGS.datadir:
        raise EnvironmentError("You have to give a data directory.")


    # -*-                     Populating the database                     -*- #

    # Iterates on every directory in datadir (each directory holds the data for
    # sequence)
    for directory in os.listdir(ARGS.datadir):
        # Creates the path to the current directory (which is handy)
        cur_dir = os.path.join(ARGS.datadir, directory)
        # Initializes empty parameters
        md_parameters = {}
        label = ""
        fasta_file = None
        # Iterates on every file in current directory
        for filepath in os.listdir(cur_dir):
            if filepath == "sequence.fasta":
                fasta_file = os.path.join(cur_dir, filepath)
            if filepath == "label":
                label = open(os.path.join(cur_dir, filepath)).readlines()[0].strip("\n")
            if ".dat" in filepath:
                md_parameters[filepath.replace(".dat", '')] = os.path.join(cur_dir, filepath)

        # Fasta file is mandatory (a sequence is required)
        if fasta_file is None:
            raise NoFastaProvided("There is no fasta file in %r" % (cur_dir,))
        # Label file is mandatory (label is mandatory for supervised learning)
        if label is None:
            raise NoLabelProvided("There is no label file in %r" % (cur_dir,))

        # Tells the users that something is happening (some users do like that)
        print "Processing %r" % (cur_dir,)

        # Creates a Sequence object
        seq = sequence.Sequence(fasta_file, md_parameters, label, alphabet=ARGS.alphabet, trx_scale_path=ARGS.trx_scale_file, sliding=ARGS.sliding, centering=ARGS.centering, alpha=ARGS.alpha, graph=ARGS.graph)
        seq_id, label = DBCURSOR.check_sequence(seq)
        if seq_id:
            if raw_input("This sequence is already in database. Would you like to recompute its data? [y/n]") != "y":
                continue
            else:
                DBCURSOR.remove_seq(seq_id)
        seq.do_analysis()

        # Tells the users that something is happening (some users do like that)
        print "Adding data stored in %r in the database" % (cur_dir,)

        # Stores the sequence "seq" in the database
        seq_id = DBCURSOR.store_sequence(seq)

        # Done, moving on to next directory
        print "Data stored in %r added to database under id %s" % (cur_dir, seq_id)

    # Exiting for loop, end
    print "Database successfully populated. Congratulations. You may proceed"
