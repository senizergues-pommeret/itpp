#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Main module of the **Xylok** project.
It defines a *class* that creates a sequence object which contains the
sequence information needed to carry out the project:

  * sequence alphabet (DNA, RNA, protein)
  * name
  * accession number
  * description
  * sequence
  * path to TRX scale
  * smooth window AKA *sliding*
  * number of bps considered as the sequence center
  * alpha parameter of the Student table (statistical computation)
  * label (supervised learning)

Moreover, the object stores results such as:

  * meaningful values of the Fast Fourier Transform of the TRX signal
    (on complete signal, signal's center and smoothed signal)
  * meaningful values of the Fast Fourier Transform of the helicoidal
    parameters' signal
  * answers to statistical tests of correlation between frames on
    every possible pair of helicoidal parameters

Defines:
  - :py:class:`Sequence`

Requires:
  - :py:mod:`lib.file_tools`
  - :py:mod:`lib.fft_tools`
  - :py:mod:`lib.correlation`
  - :py:mod:`lib.trx`

"""

import lib.file_tools as file_tools
import lib.fft_tools as fft_lib
import lib.correlation as correlation_lib
import lib.trx as trx_lib

class Sequence(object):
    """
    Class that handles a sequence.
    """

    def __init__(self, fasta, md_parameters, label="", alphabet="dna", trx_scale_path=trx_lib.SCALE_FILE, sliding=72, centering=72, alpha=0.05, graph=None):
        """
        Initializes Sequence class
        """
        if not alphabet:
            # EnvironmentError: error used when not enough information was given
            raise EnvironmentError("Sequence alphabet is mandatory.")
        if not fasta:
            # EnvironmentError: error used when not enough information was given
            raise EnvironmentError("Giving a FASTA file is mandatory.")

        # self.alphabet stores the sequence alphabet ("dna", "rna" or "prot"),
        # default: "dna"
        self.alphabet = alphabet
        # self.sliding stores the sliding window used to smooth data
        # default: 72 bp
        self.sliding = sliding
        # self.alpha stores the alpha parameter that is to be used to retrieve
        # statistical value in the Student table, default: 0.05
        self.alpha = alpha
        # self.centering stores the number of bp that are ought to be
        # considered as center, default: 72 bp
        self.centering = centering
        # self.label stores the label required for supervised learning
        self.label = label
        # self.graph stores how many graphs are to be plotted in the FFT part
        # of the programe, default: None
        self.graph = graph
        # self.trx_scale_path stores the path to the TRX scale that is to be
        # used in the TRX part of the program
        self.trx_scale_path = trx_scale_path

        # self.accession stores the accession number of the sequence (retrieved
        # from the fasta file)
        self.accession = ""
        # self.sequence stores the sequence (retrieved from the fasta file)
        self.sequence = ""
        # self.name stores the name of the sequence (retrieved from the fasta
        # file)
        self.name = ""
        # self.name stores the description of the sequence (retrieved from the
        # fasta file)
        self.description = ""
        # self.mdd stores the meaningful values from the processing of the MD
        # data stored in the .dat files
        self.mdd = {}
        # self.correlation stores the answers to the Spearman and Pearson tests
        # processed on every pair of helicoidal parameters, data come from the
        # .dat files
        self.correlation = {}
        # self.trx stores the meaningful values from the processing of
        # self.sequence using self.trx_scale_path
        self.trx = {}

        # Stores md_parameters for computation
        self.md_parameters = md_parameters

        # Populates some of the attributes
        self.load_fasta(fasta)

    def do_analysis(self):
        """
        Runs function and triggers the actual job.
        *(Those functions are called in a specific function so that they are
        called only if the data isn't already in the database)*

        Parameters:
          - ``self``
        Uses:
          - :py:meth:`load_md`
          - :py:meth:`load_trx`
        Returns:
          - nothing
        """
        self.load_md()
        self.load_trx()

    def get(self, name, default):
        """
        Tries to return self[name], if it fails, returns default value "".

        Parameters:
          - ``self``
          - ``name``
          - ``default``
        Returns:
          - a string
        May raise:
          - AttributeError
        """
        try:
            return getattr(self, name)
        except AttributeError:
            return default

    def __getitem__(self, name):
        """
        Tries to fetch self.a when self["a"] is called in the code. If it fails
        it raises a KeyError (because that's what dict["a"] is supposed to
        return if "a" is not a key).

        Parameters:
          - ``self``
          - ``name``
        Returns:
          - self["a"]
        May raise:
          - KeyError
        """
        try:
            return getattr(self, name)
        except AttributeError:
            raise KeyError('%r' % (name,))

    def load_fasta(self, fasta):
        """
        Loads a sequence from a fasta file and populates:
          - ``self.sequence``
          - ``self.name``
          - ``self.accession``
          - ``self.description``

        Parameters:
          - ``self``
          - ``fasta`` : str, path to the considered fasta file
        Uses:
          - :py:meth:`lib.file_tools:load_fasta`
        Returns:
          - nothing
        """
        parsed_sequence = file_tools.load_fasta(self.alphabet, fasta)
        self.sequence = str(parsed_sequence.seq)
        self.name = str(parsed_sequence.name)
        self.accession = str(parsed_sequence.id.split("|")[3])
        self.description = str(parsed_sequence.description)

    def load_md(self):
        # XXX
        """
        Loads MD files and populates:
          - ``self.mdd``
          - ``self.correlation``

        Parameters:
          - ``self``
        Uses:
          - :py:meth:`lib.file_tools:load_md_data`
          - :py:meth:`lib.fft_tools:fft`
          - :py:meth:`lib.fft_tools:get_noticeable_data`
          - :py:meth:`lib.correlation:compute_correlations`
        Returns:
          - nothing
        May raise:
          - NotImplementedError
        """

        _md_params = {}

        # -*-                     Populating self.mdd                     -*- #
        # Iterates on every helicoidal parameter file
        for (helicoidal_parameter, path) in self.md_parameters.iteritems():
            # Loads the file
            _md_params[helicoidal_parameter] = file_tools.load_md_data(path)

            # Stores Fast Fourier Transform results in temp objects
            # _md_ffts is a list of 3-tuples: (frame_number, freqs, transform)
            _md_ffts = [fft_lib.fft(frame) for frame in _md_params[helicoidal_parameter]]

            # Stores Fast Fourier Transform results of the sequence's center
            _md_ffts_c = [fft_lib.fft(frame, centering=self.centering) for frame in _md_params[helicoidal_parameter]]

            # Stores Fast Fourier Transform results of the "sliding" sequence
            _md_ffts_s = [fft_lib.sliding_fft(frame, window=self.sliding) for frame in _md_params[helicoidal_parameter]]

            # Retrieves meaningful values for each type of results
            # Type of results: complete, center and sliding
            good_values = [fft_lib.get_noticeable_data(fft) for fft in _md_ffts]
            good_values_c = [fft_lib.get_noticeable_data(fft) for fft in _md_ffts_c]
            good_values_s = [fft_lib.get_noticeable_data(fft) for fft in _md_ffts_s]

            # Populates self.mdd with those results
            # NB: _md_ffts[i][0] stores the frame number
            self.mdd[helicoidal_parameter] = {
                _md_ffts[i][0] : {
                    'complete_peak_freq' : good_values[i][0],
                    'complete_peak' : good_values[i][1],
                    'complete_size' : good_values[i][2],
                    'center_peak_freq' : good_values_c[i][0],
                    'center_peak' : good_values_c[i][1],
                    'center_size' : good_values_c[i][2],
                    'sliding_peak_freq' : good_values_s[i][0],
                    'sliding_peak' : good_values_s[i][1],
                    'sliding_size' : good_values_s[i][2],
                    } for i in xrange(0, len(_md_ffts))
                }

            if self.graph is not None:
                raise NotImplementedError("Plotting is not implemented yet")
            #    fft_lib.plot(_md_ffts, _md_ffts_c, _md_ffts_s, self.graph, helicoidal_parameter)


        # -*-                 Populating self.correlation                 -*- #

        # In order to compute every frame correlation between 2 parameters,
        # _md_params must be kept in python's memory

        # To compute every "pair" correlation once and only one time, a double
        # loop is required. It runs amongst possible helicoidal_parms values
        # that are lexicographically-ordered.
        helicoidal_parameters = [] + _md_params.keys()
        helicoidal_parameters.sort()
        helicoidal_parameters2 = [] + helicoidal_parameters

        for param in helicoidal_parameters:
            helicoidal_parameters2.remove(param)
            for param2 in helicoidal_parameters2:
                self.correlation[param + "/" + param2] = correlation_lib.compute_correlations(_md_params[param], _md_params[param2], param, param2, alpha=self.alpha, centering=self.centering)


    def load_trx(self):
        """
        Populates:
          - ``self.trx``

        Parameters:
          - ``self``
        Uses:
          - :py:meth:`lib.trx:match`
          - :py:meth:`lib.trx:parse_trx_scale`
          - :py:meth:`lib.fft_tools:fft`
          - :py:meth:`lib.fft_tools:sliding_fft`
          - :py:meth:`lib.fft_tools:get_noticeable_data`
        Returns:
          - nothing
        """
        # Translates self.sequence in TRX values
        trx_dict = trx_lib.match(self.sequence, trx_lib.parse_trx_scale(self.trx_scale_path))

        # Stores the Fast Fourier Transform in temp objects
        _fft = fft_lib.fft(trx_dict)

        # Stores the Fast Fourier Transform of sequence's center
        _fft_c = fft_lib.fft(trx_dict, centering=self.centering)

        # Stores the Fast Fourier Transform of the "sliding" sequence
        _fft_s = fft_lib.sliding_fft(trx_dict, window=self.sliding)

        # Retrieves meaningful values for each type of results
        # Type of results: complete, center and sliding
        good_values = fft_lib.get_noticeable_data(_fft)
        good_values_c = fft_lib.get_noticeable_data(_fft_c)
        good_values_s = fft_lib.get_noticeable_data(_fft_s)

        # Populates self.trx with those results
        self.trx = {
            'complete_peak_freq' : good_values[0],
            'complete_peak' : good_values[1],
            'complete_size' : good_values[2],
            'center_peak_freq' : good_values_c[0],
            'center_peak' : good_values_c[1],
            'center_size' : good_values_c[2],
            'sliding_peak_freq' : good_values_s[0],
            'sliding_peak' : good_values_s[1],
            'sliding_size' : good_values_s[2],
            }
