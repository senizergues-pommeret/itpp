-- Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
-- Authors : Pauline Pommeret <pommeret@crans.org>,
--           Jonas Senizergues <senizergues@crans.org>
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
-- * Redistributions of source code must retain the above copyright
--   notice, this list of conditions and the following disclaimer.
-- * Redistributions in binary form must reproduce the above copyright
--   notice, this list of conditions and the following disclaimer in the
--   documentation and/or other materials provided with the distribution.
-- * Neither the name of the Cr@ns nor the names of its contributors may
--   be used to endorse or promote products derived from this software
--   without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
-- "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
-- LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
-- A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
-- HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
-- EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
-- PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
-- PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
-- LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
-- NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
CREATE TABLE "sequences" (
    id SERIAL,
    "accession" CHARACTER VARYING(20),
    "name" CHARACTER VARYING(300) NOT NULL,
    "description" TEXT NOT NULL,
    "sequence" TEXT NOT NULL,
    "label" CHARACTER VARYING(50),
    "alphabet" CHARACTER VARYING(5) NOT NULL,
    "alpha" REAL NOT NULL,
    "sliding" SMALLINT NOT NULL,
    "centering" SMALLINT NOT NULL,
    "trx_scale_path" CHARACTER VARYING(500) NOT NULL,
    CONSTRAINT "sequences_pkey" PRIMARY KEY("id")
) WITHOUT OIDS;

CREATE TABLE "md_rise" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "md_roll" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "md_shift" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "md_slide" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "md_tilt" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "md_twist" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "trx" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "complete_peak_freq" REAL NOT NULL,
    "complete_peak" REAL NOT NULL,
    "complete_size" REAL NOT NULL,
    "center_peak_freq" REAL NOT NULL,
    "center_peak" REAL NOT NULL,
    "center_size" REAL NOT NULL,
    "sliding_peak_freq" REAL NOT NULL,
    "sliding_peak" REAL NOT NULL,
    "sliding_size" REAL NOT NULL
);

CREATE TABLE "correlations" (
    "seq_id" INTEGER NOT NULL REFERENCES "sequences",
    "frame_num" INTEGER NOT NULL,
    "type_a" CHARACTER VARYING(6) NOT NULL,
    "type_b" CHARACTER VARYING(6) NOT NULL,
    "spearman_complete" BOOLEAN NOT NULL,
    "spearman_center" BOOLEAN NOT NULL,
    "pearson_complete" BOOLEAN NOT NULL,
    "pearson_center" BOOLEAN NOT NULL
);
