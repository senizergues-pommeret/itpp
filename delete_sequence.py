#!/usr/bin/env python2.7
#
# Copyright (C) 2014 Pauline Pommeret <pommeret@crans.org>
# Authors : Pauline Pommeret <pommeret@crans.org>,
#           Jonas Senizergues <senizergues@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Delete a sequence from database.

"""

import argparse
from argparse import RawDescriptionHelpFormatter

import lib.database


if __name__ == "__main__":
    DBCURSOR = lib.database.XylokPGCursor()

    # -*-          Parsing command-line arguments using argparse          -*- #

    # TEXT is a string that holds the description of the program
    TEXT = """Delete sequence from database."""

    # Creates the parser
    PARSER = argparse.ArgumentParser(description=TEXT, formatter_class=RawDescriptionHelpFormatter)

    # Fills the parser with the program arguments
    PARSER.add_argument("seqid", type=int, help="The id of the sequence to remove.", action="store")

    # Parses the arguments
    ARGS = PARSER.parse_args()

    # Prints something
    if DBCURSOR.remove_seq(ARGS.seqid) >= 1:
        print "Sequence %s has been successfully removed." % (ARGS.seqid,)
    else:
        print "Sequence %s does not exist." %(ARGS.seqid,)
